FROM amazonlinux:latest

RUN yum update -y
RUN yum install net-tools -y
RUN yum install httpd -y
COPY ./index.html /var/www/html/
RUN chmod 777 /var/www/html/index.html
ENTRYPOINT ["/usr/sbin/httpd", "-D", "FOREGROUND"]
